<?php
/**
 * Plugin Name: ClusterPress Accwoont
 * Plugin URI: https://cluster.press
 * Description: Ce cluster requiert woocommerce et remplace la page du compte client de woocommerce par une nouvelle section dans le profil ClusterPress.
 * Version: 1.0.0
 * License: GPLv2 or later (license.txt)
 * Author: imath
 * Author URI: https://imathi.eu/
 * Text Domain: clusterpress-accwoont
 * Domain Path: /languages/
 * Network: True
 * Default Language: fr_FR
 * cluster_id: accwoont
 * cluster_name: Compte Client
 * cluster_icon: img/accwoont.png
 * cluster_requires: 4.7
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Only Load the Woo Account Cluster files and classes if on the main site.
 *
 * @since  1.0.0
 *
 * @param  string $dir     Path to the Cluster's directory.
 * @param  string $cluster ID of the Cluster being loaded.
 * @return string          Path to the Accwoont Cluster directory.
 */
function cpwoo_set_cluster_dir( $dir = '', $cluster = '' ) {
	if ( empty( $cluster ) || 'accwoont' !== $cluster || ! cp_is_main_site() ) {
		return $dir;
	}

	return plugin_dir_path( __FILE__ );
}
add_filter( 'cp_custom_cluster_includes_dir', 'cpwoo_set_cluster_dir',  10, 2 );

/**
 * Edit the Cluster's object to add a woocommerce (wc) property
 *
 * This property is used to check woocommerce is available and loaded.
 *
 * @since  1.0.0
 */
function cpwoo_commerce_is_loaded( &$cluster ) {
	if ( empty( $cluster->id ) || 'accwoont' !== $cluster->id ) {
		return;
	}

	$cluster->wc_loaded = true;
}

/**
 * Wait for woocommerce and add a specific global to the Cluster.
 *
 * Also, add the plugin's cluster name strings so that it can be translated.
 *
 * @since  1.0.0
 */
function clusterpress_accwoont() {
	// Used in the plugin's header.
	$cluster_name = __( 'Compte Client', 'clusterpress-accwoont' );

	add_action( 'cp_accwoont_cluster_globals', 'cpwoo_commerce_is_loaded', 10, 1 );
}
add_action( 'woocommerce_loaded', 'clusterpress_accwoont' );
