<?php
/**
 * User profile Template Tags
 *
 * @since  1.0.0
 *
 * @package ClusterPress Accwoont\accwoont
 * @subpackage tags
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Display the user's account content
 *
 * @since 1.0.0
 *
 * @return string HTML Output
 */
function cpwoo_display_user_screens() {
	if ( ! cp_is_user() ) {
		return;
	}

	// Prevents the nav to be displayed
	remove_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );

	// We're on a woocommerce account page
	add_filter( 'woocommerce_is_account_page', '__return_true' );

	echo cpwoo_get_user_screens_content();

	// Restore the action just in case
	add_action( 'woocommerce_account_navigation', 'woocommerce_account_navigation' );

	// We're no more on a woocommerce account page!
	remove_filter( 'woocommerce_is_account_page', '__return_true' );
}


	/**
	 * Get the user's account content
	 *
	 * @since 1.0.0
	 *
	 * @return string The post content for my-account's page.
	 */
	function cpwoo_get_user_screens_content() {

		// Get the current post to be sure the Post global is set
		$reset_post = get_post();

		// Get the My account page and temporarly set it as the current post
		$GLOBALS['post'] = get_post( wc_get_page_id( 'myaccount' ) );
		setup_postdata( $GLOBALS['post'] );

		// Use the content in case the admin has added some text to the page.
		$content = get_the_content();

		if ( empty( $content ) ) {
			$content = '[woocommerce_my_account]';
		}

		// Reset the current post
		wp_reset_postdata();
		$GLOBALS['post'] = $reset_post;

		return apply_filters( 'cpwoo_get_user_screens_content', $content );
	}
