<?php
/**
 * Filters
 *
 * @since  1.0.0
 *
 * @package ClusterPress Accwoont\accwoont
 * @subpackage filters
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

add_filter( 'cpwoo_get_user_screens_content', 'capital_P_dangit',                  11 );
add_filter( 'cpwoo_get_user_screens_content', 'wptexturize'                           );
add_filter( 'cpwoo_get_user_screens_content', 'wp_unslash'                            );
add_filter( 'cpwoo_get_user_screens_content', 'convert_smilies',                   20 );
add_filter( 'cpwoo_get_user_screens_content', 'wpautop'                               );
add_filter( 'cpwoo_get_user_screens_content', 'shortcode_unautop'                     );
add_filter( 'cpwoo_get_user_screens_content', 'wp_make_content_images_responsive'     );
add_filter( 'cpwoo_get_user_screens_content', 'do_shortcode',                      12 );

/**
 * Set ClusterPress profile urls if needed.
 *
 * @since  1.0.0
 *
 * @param  string $url       The url for the endpoint.
 * @param  string $endpoint  The endpoint of the user action.
 * @param  string $value     A slug or an ID of order.
 * @return string            The url for the endpoint.
 */
function cpwoo_get_endpoint_url( $url = '', $endpoint = '', $value = '' ) {
	$cp = clusterpress();

	if ( ! isset( $cp->user->toolbar ) )  {
		return $url;
	}

	$woo_endpoint = cpwoo_get_wc_query_vars( $endpoint, true );

	if ( 'edit-account' === $woo_endpoint ) {
		$endpoint = cp_user_get_manage_account_slug();
	} elseif ( 'view-order' === $woo_endpoint ) {
		$endpoint = cpwoo_get_wc_query_vars( 'orders' );
	}

	$node = $cp->user->toolbar->get_node_by_slug( $endpoint );

	if ( empty( $node->href ) ) {
		return $url;
	} else {
		$url = $node->href;
	}

	if ( empty( $value ) ) {
		return $url;
	}

	$slug = sanitize_key( $value );
	$slug = wc_edit_address_i18n( $slug, true );

	// Check for a specific address type
	if ( 'edit-address' === $woo_endpoint && $slug && is_callable( 'cpwoo_get_user_' . $slug . '_slug' ) ) {
		if ( $cp->permalink_structure ) {
			$url = trailingslashit( $url ) . call_user_func( 'cpwoo_get_user_' . $slug . '_slug' );
		} else {
			$url = add_query_arg( 'cpwoo_' . $slug, 1, $url );
		}

	// Check for a specific order
	} elseif ( 'view-order' === $woo_endpoint && is_numeric( $value ) ) {
		if ( $cp->permalink_structure ) {
			$url = trailingslashit( $url ) . cpwoo_get_user_order_slug() . '/' . $value;
		} else {
			$url = add_query_arg( 'cpwoo_order', $value, $url );
		}

	// Check pagination
	} elseif ( 'orders' === $woo_endpoint && is_numeric( $value ) ) {
		if ( $cp->permalink_structure ) {
			$url = trailingslashit( $url ) . cp_get_paged_slug() . '/' . $value;
		} else {
			$url = add_query_arg( 'paged', $value, $url );
		}
	}

	return $url;
}
add_filter( 'woocommerce_get_endpoint_url', 'cpwoo_get_endpoint_url', 10, 3 );
