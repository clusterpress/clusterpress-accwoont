<?php
/**
 * The Cluster's class.
 *
 * @since  1.0.0
 *
 * @package ClusterPress Accwoont\accwoont\classes
 * @subpackage CP Accwoont Cluster
 *
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * The Accwoont Cluster class.
 */
class CP_Accwoont_Cluster extends CP_Cluster {

	/**
	 * Constructor.
	 *
	 * @since  1.0.0
	 *
	 * @param array $args The Cluster's parameters.
	 */
	public function __construct( $args = array() )  {
		parent::__construct( $args );
	}

	/**
	 * Instantiate the Cluster if not set yet.
	 *
	 * @since  1.0.0
	 *
	 * @return CP_Accwoont_Cluster This class.
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->accwoont ) ) {
			$cp->accwoont = new self( array(
				'id'            => 'accwoont',
				'name'          => __( 'Compte Client', 'clusterpress-accwoont' ),
				'absdir'        => plugin_dir_path( dirname( __FILE__ ) ),
			) );

			$cp->accwoont->version   = '1.0.0';
			$plugin_basename       = dirname( dirname( __FILE__ ) );
			$cp->accwoont->path_data = array(
				'dir' => plugin_dir_path( $plugin_basename ),
				'url' => plugin_dir_url ( $plugin_basename ),
			);
		}

		return $cp->accwoont;
	}

	/**
	 * Includes the needed files.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $files The list of files/classes to include
	 */
	public function load_cluster( $files = array() ) {
		if ( empty( $this->wc_loaded ) ) {
			return;
		}

		$files['files'] = array(
			'functions',
			'tags',
			'filters',
			'actions',
		);

		parent::load_cluster( $files );
	}

	/**
	 * Sets specific hook to the Cluster.
	 *
	 * @since  1.0.0
	 */
	public function set_cluster_hooks() {
		// Load languages
		add_action( 'cp_include_clusters', array( $this, 'load_languages' ) );

		if ( empty( $this->wc_loaded ) ) {
			add_action( 'cp_admin_notices', array( $this, 'admin_notice' ) );
			return;
		}

		// Eventually upgrade the plugin
		add_action( 'cp_admin_init', array( $this, 'plugin_upgrade' ), 999 );
	}

	/**
	 * Displays a notice if woocommerce is not active on the main site.
	 *
	 * @since  1.0.0
	 *
	 * @return HTML Output.
	 */
	public function admin_notice() {
		$notices = array(
			esc_html__( 'ClusterPress Accwoont nécessite que l\'extension woocommerce soit activée sur le site principal.', 'clusterpress-accwoont' ),
			esc_html__( 'Merci d\'activer woocommerce ou de désactiver ce Cluster.', 'clusterpress-accwoont' ),
		);

		printf( '<div class="error notice is-dismissible">%s</div>', '<p>' . join( '</p><p>', $notices ) . '</p>' );
	}

	/**
	 * Parse the query to set the Woo Account object.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Query $q The WP Query object.
	 */
	public function parse_query( $q = null ) {
		if ( ! cp_can_alter_posts_query( $q ) || empty( $this->wc_loaded ) ) {
			return;
		}

		// Get woocommerce main instance.
		$wc = WC();

		if ( ! isset( $wc->query->query_vars ) ) {
			return;
		}

		// Redirect to the ClusterPress profile
		if ( cpwoo_is_account_page( $q ) && is_user_logged_in() ) {
			$account_vars = array_intersect_key( $q->query, $wc->query->query_vars );

			$redirect_url = cpwoo_redirect_my_account_url( $account_vars, $wc->query->get_current_endpoint() );

			if ( ! empty( $redirect_url ) ) {
				wp_redirect( $redirect_url );
				exit();
			}
			return;
		}

		if ( ! cp_is_user() ) {
			return;
		}

		// woocommerce is using this global to get requested my account sub pages.
		global $wp;

		$wc_query_vars = array();
		$q_key         = 'cp_woo_account';

		if ( cp_is_user_manage() ) {
			$q_key = cp_user_get_manage_rewrite_id();
		}

		foreach( (array) array_flip( $wc->query->query_vars ) as $kqv => $vqv ) {
			if ( $kqv === $q->get( $q_key ) ) {
				$q->set( $vqv, 1 );
				$wp->query_vars[ $vqv ] = '';
			}
		}

		if ( isset( $wp->query_vars['orders'] ) ) {
			$order = $q->get( 'cpwoo_order' );
			$paged = $q->get( 'paged' );

			// Specific order
			if ( ! empty( $order ) ) {
				$wp->query_vars['view-order'] = (int) $order;
				unset( $wp->query_vars['orders'] );

			// Paginate orders
			} elseif ( ! empty( $paged ) ) {
				$wp->query_vars['orders'] = (int) $paged;
			}
		}

		if ( isset( $wp->query_vars['edit-address'] ) ) {
			$address_type = $q->get( 'cpwoo_billing' );

			if ( empty( $address_type ) && 1 === (int) $q->get( 'cpwoo_shipping' ) ) {
				$address_type = 'shipping';

			} elseif ( ! empty( $address_type ) ) {
				$address_type = 'billing';
			}

			if ( $address_type ) {
				$wp->query_vars['edit-address'] = wc_edit_address_i18n( $address_type );
			}
		}

		parent::parse_query( $q );
	}

	/**
	 * Register the rewrite tags for the Cluster.
	 *
	 * NB: This is only happening on multisite configs and if the site's Cluster is enabled.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $tags The list of rewrite tags.
	 * @return array        The list of rewrite tags.
	 */
	public function rewrite_tags( $tags = array() ) {
		if ( empty( $this->wc_loaded ) ) {
			parent::rewrite_tags( array() );
			return;
		}

		parent::rewrite_tags( array(
			array(
				'tag'   => '%cpwoo_order%',
				'regex' => '([^/]+)',
			),
			array(
				'tag'   => '%cpwoo_billing%',
				'regex' => '([1]{1,})',
			),
			array(
				'tag'   => '%cpwoo_shipping%',
				'regex' => '([1]{1,})',
			),
		) );
	}

	/**
	 * Register the rewrite rules for the Cluster.
	 *
	 * NB: This is only happening on multisite configs and if the site's Cluster is enabled.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $rules The list of rewrite rules.
	 * @return array         The list of rewrite rules.
	 */
	public function rewrite_rules( $rules = array() ) {
		if ( empty( $this->wc_loaded ) ) {
			parent::rewrite_rules( array() );
			return;
		}

		$account_base       = trailingslashit( cp_get_root_slug() ) . cp_user_get_slug();
		$account_regex      = $account_base . '/([^/]+)/' . cpwoo_get_user_account_slug();
		$edit_account_regex = $account_base . '/([^/]+)/' . cp_user_get_manage_slug();

		parent::rewrite_rules( array(
			array(
				'regex' => $edit_account_regex . '/([^/]+)/' . cpwoo_get_user_shipping_slug() . '/?$',
				'query' => 'index.php?' . cp_user_get_rewrite_id() . '=$matches[1]&' . cp_user_get_manage_rewrite_id() . '=$matches[2]&cpwoo_shipping=1',
			),
			array(
				'regex' => $edit_account_regex . '/([^/]+)/' . cpwoo_get_user_billing_slug() . '/?$',
				'query' => 'index.php?' . cp_user_get_rewrite_id() . '=$matches[1]&' . cp_user_get_manage_rewrite_id() . '=$matches[2]&cpwoo_billing=1',
			),
			array(
				'regex' => $account_regex . '/([^/]+)/' . cp_get_paged_slug() . '/([^/]+)/?$',
				'query' => 'index.php?' . cp_user_get_rewrite_id() . '=$matches[1]&cp_woo_account=$matches[2]&paged=$matches[3]',
			),
			array(
				'regex' => $account_regex . '/([^/]+)/' . cpwoo_get_user_order_slug() . '/([^/]+)/?$',
				'query' => 'index.php?' . cp_user_get_rewrite_id() . '=$matches[1]&cp_woo_account=$matches[2]&cpwoo_order=$matches[3]',
			),
		) );
	}

	/**
	 * Upgrade the plugin if needed.
	 *
	 * @since  1.0.0
	 */
	public function plugin_upgrade() {
		$db_version = get_network_option( 0, '_clusterpress_accwoont_version', 0 );

		if ( version_compare( $db_version, $this->version, '<' ) ) {
			update_network_option( 0, '_clusterpress_accwoont_version', $this->version );
		}
	}

	/**
	 * Load Translations.
	 *
	 * @since 1.0.0
	 */
	public function load_languages() {
		$mofile = sprintf( $this->path_data['dir'] . 'languages/clusterpress-accwoont-%s.mo', get_locale() );
		cp_load_textdomain( 'clusterpress-accwoont', $mofile );
	}
}
