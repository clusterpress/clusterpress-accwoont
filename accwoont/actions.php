<?php
/**
 * Actions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Accwoont\accwoont
 * @subpackage actions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Registers the displayed user's profile Forum section.
 *
 * @since  1.0.0
 */
function cpwoo_register_user_section() {
	$woo_tabs  = array();
	$woo_qv    = cpwoo_get_wc_query_vars();
	$woo_items = wc_get_account_menu_items();

	// Merge the user dashboard
	$woo_qv = array_merge( array( 'dashboard' => cpwoo_get_user_dashboard_slug() ), $woo_qv );

	$disabled = apply_filters( 'cpwoo_disabled_user_sections', array(
		'edit-address'    => true,
		'edit-account'    => true,
		'customer-logout' => true,
	) );

	foreach ( $woo_items as $kw => $tw ) {
		if ( ! empty( $disabled[ $kw ] ) || empty( $woo_qv[ $kw ] ) ) {
			continue;
		}

		$position = 10;

		$woo_tabs[] = array(
			'id'           => 'woo-' . $kw,
			'slug'         => $woo_qv[ $kw ],
			'parent'       => 'cp_woo_account',
			'parent_group' => 'single-bar',
			'title'        => $tw,
			'href'         => '#',
			'position'     => $position,
			'capability'   => 'cp_edit_single_user',
		);

		$position += 10;
	}

	// Registers The regular User's profile section
	cp_register_cluster_section( 'user', array(
		'id'               => 'cp-accwoont',
		'name'             => cpwoo_get_user_account_name(),
		'cluster_id'       => 'user',
		'slug'             => cpwoo_get_user_account_slug(),
		'rewrite_id'       => 'cp_woo_account',
		'display_callback' => 'cpwoo_display_user_screens',
		'toolbar_items'    => array(
			'position'     => 80,
			'capability'   => 'cp_edit_single_user',
			'dashicon'     => 'dashicons-cart',
			'children'     => $woo_tabs,
		),
	) );

	// Registers The "manage" User's profile section
	if ( isset( $woo_qv['edit-address'] ) ) {
		cp_register_cluster_section( 'user', array(
			'id'               => 'cp-manage-accwoont',
			'name'             => $woo_items['edit-address'],
			'cluster_id'       => 'user',
			'slug'             => cpwoo_get_user_edit_address_slug(),
			'rewrite_id'       => 'cp_woo_edit_address',
			'display_callback' => 'cpwoo_display_user_screens',
			'toolbar_items'    => array(
				'position'     => 30,
				'capability'   => 'cp_edit_single_user',
				'parent'       => cp_user_get_manage_rewrite_id(),
			),
		) );
	}
}
add_action( 'cp_register_cluster_sections', 'cpwoo_register_user_section', 14 );

/**
 * Redirect the user to a on the address manage tab on successfull edits.
 *
 * @since  1.0.0
 */
function cpwoo_customer_redirect_after_saved_address() {
	if ( ! cp_is_user_manage() ) {
		return;
	}

	$redirect = cp_user_get_url( array(
		'slug'       => cp_user_get_manage_slug() . '/' . cpwoo_get_user_edit_address_slug(),
		'rewrite_id' => cp_user_get_manage_rewrite_id(),
	), cp_displayed_user() );

	if ( ! $redirect ) {
		return;
	}

	wp_safe_redirect( $redirect );
	exit;
}
add_action( 'woocommerce_customer_save_address', 'cpwoo_customer_redirect_after_saved_address' );
