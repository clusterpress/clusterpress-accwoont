<?php
/**
 * Functions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Accwoont\accwoont
 * @subpackage functions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get the plugin's version.
 *
 * @since  1.0.0
 *
 * @return string The plugin's version.
 */
function cpwoo_get_plugin_version() {
	return clusterpress()->accwoont->version;
}

/**
 * Get the user's shop name.
 *
 * @since  1.0.0
 *
 * @return string The user's shop name.
 */
function cpwoo_get_user_account_name() {
	/**
	 * Filter here to edit the shop name.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the shop name.
	 */
	return apply_filters( 'cpwoo_get_user_account_name', _x( 'Boutique', 'name user account', 'clusterpress-accwoont' ) );
}

/**
 * Get the user's shop slug.
 *
 * @since  1.0.0
 *
 * @return string The user's shop slug.
 */
function cpwoo_get_user_account_slug() {
	/**
	 * Filter here to edit the shop slug.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the shop slug.
	 */
	return apply_filters( 'cpwoo_get_user_account_slug', _x( 'boutique', 'slug user account', 'clusterpress-accwoont' ) );
}

/**
 * Get the user's dashboard slug.
 *
 * @since  1.0.0
 *
 * @return string The user's dashboard slug.
 */
function cpwoo_get_user_dashboard_slug() {
	/**
	 * Filter here to edit the dashboard slug.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value the dashboard slug.
	 */
	return apply_filters( 'cpwoo_get_user_dashboard_slug', _x( 'accueil', 'slug user dashboard', 'clusterpress-accwoont' ) );
}

/**
 * Get the user's account order slug.
 *
 * @since  1.0.0
 *
 * @return string The user's account order slug.
 */
function cpwoo_get_user_order_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value The user's account order slug.
	 */
	$slug = apply_filters( 'cpwoo_get_user_order_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( 'woocommerce_myaccount_view_order_endpoint', 'view-order' );
}

/**
 * Get the user's account edit address slug.
 *
 * @since  1.0.0
 *
 * @return string The user's account edit address slug.
 */
function cpwoo_get_user_edit_address_slug() {
	/**
	 * Filter here to avoid a db query.
	 *
	 * @since  1.0.0
	 *
	 * @param string $value The user's account edit address slug.
	 */
	$slug = apply_filters( 'cpwoo_get_user_edit_address_slug', '' );

	if ( ! empty( $slug ) ) {
		return $slug;
	}

	return get_option( 'woocommerce_myaccount_edit_address_endpoint', 'edit-address' );
}

/**
 * Get the user's account billing address slug.
 *
 * @since  1.0.0
 *
 * @return string The user's account billing address slug.
 */
function cpwoo_get_user_billing_slug() {
	$slug = wc_edit_address_i18n( 'billing' );

	/**
	 * Filter here to edit the billing address slug.
	 *
	 * @since  1.0.0
	 *
	 * @param string $slug the billing address slug.
	 */
	return apply_filters( 'cpwoo_get_user_billing_slug', $slug );
}

/**
 * Get the user's account shipping address slug.
 *
 * @since  1.0.0
 *
 * @return string The user's account shipping address slug.
 */
function cpwoo_get_user_shipping_slug() {
	$slug = wc_edit_address_i18n( 'shipping' );

	/**
	 * Filter here to edit the shipping address slug.
	 *
	 * @since  1.0.0
	 *
	 * @param string $slug the shipping address slug.
	 */
	return apply_filters( 'cpwoo_get_user_shipping_slug', $slug );
}

/**
 * Check the requested page is the woocommerce Account page.
 *
 * @since 1.0.0
 *
 * @param  WP_Query $q The WordPress main query object.
 * @return bool        True if the requested page is the woocommerce account page.
 *                     False otherwise.
 */
function cpwoo_is_account_page( $q = null ) {
	if ( ! is_a( $q, 'WP_Query' ) ) {
		return false;
	}

	$account_page_id = wc_get_page_id( 'myaccount' );
	if ( ! $account_page_id ) {
		return false;
	}

	$qo = $q->get_queried_object();

	// Prevent a WordPress notice if ugly permalink are in use.
	if ( is_null( $qo ) && ! clusterpress()->permalink_structure ) {
		if ( empty( $_GET['page_id'] ) || (int) $account_page_id !== (int) $_GET['page_id'] ) {
			return false;
		}

		$q->post              = get_post( $account_page_id );
		$q->queried_object    = $q->post;
		$q->queried_object_id = $q->post->ID;
	}

	return is_account_page();
}

/**
 * Get all or a specific woocommerce query var.
 *
 * @since  1.0.0
 *
 * @param  string       $var  empty to get all, a specific term otherwise.
 * @param  bool         $flip whether to flip the list of query vars.
 * @return array|string       All query vars, the value of the key passed otherwise.
 */
function cpwoo_get_wc_query_vars( $var = '', $flip = false ) {
	$wc = WC();

	if ( ! isset( $wc->query->query_vars ) ) {
		return false;
	}

	if ( ! $var ) {
		return $wc->query->query_vars;
	}

	$query_vars = $wc->query->query_vars;
	if ( true === $flip ) {
		$query_vars = array_flip( $query_vars );
	}

	if ( ! isset( $query_vars[ $var ] ) ) {
		return false;
	}

	return $query_vars[ $var ];
}

/**
 * Returns the ClusterPress profile url which is mapped to the woocommerce my account one.
 *
 * @since 1.0.0
 *
 * @param  array   $args     An array of arguments.
 * @param  string  $endpoint The requested woocommerce endpoint.
 * @param  WP_User $user     The User object to get the redirect url for.
 * @return string            An empty string if no matches were found.
 *                           The ClusterPress profile url otherwise.
 */
function cpwoo_redirect_my_account_url( $args = array(), $endpoint = '', $user = null ) {
	if ( empty( $user->ID ) ) {
		$user = wp_get_current_user();
	}

	$has_pretty_link = clusterpress()->permalink_structure;
	$url             = '';

	if ( isset( $args['edit-address'] ) ) {
		$url = cp_user_get_url( array(
			'slug'       => cp_user_get_manage_slug() . '/' . cpwoo_get_user_edit_address_slug(),
			'rewrite_id' => cp_user_get_manage_rewrite_id(),
		), $user );

		$type = sanitize_key( $args['edit-address'] );

		if ( $type && is_callable( 'cpwoo_get_user_' . $type . '_slug' ) ) {
			if ( $has_pretty_link ) {
				$url = trailingslashit( $url ) . call_user_func( 'cpwoo_get_user_' . $type . '_slug' );
			} else {
				$url = add_query_arg( 'cpwoo_' . $type, 1, $url );
			}
		}
	} else {
		$wc_qv = cpwoo_get_wc_query_vars();

		$url_args = array(
			'slug'       => cpwoo_get_user_account_slug(),
			'rewrite_id' => 'cp_woo_account',
		);

		if ( 'edit-account' ===  $endpoint ) {
			$url = cp_user_get_url( array(
				'slug'       => cp_user_get_manage_slug() . '/' . cp_user_get_manage_account_slug(),
				'rewrite_id' => cp_user_get_manage_rewrite_id(),
			), $user );
		} elseif ( ! empty( $wc_qv[ $endpoint ] ) ) {
			if ( 'view-order' !== $endpoint ) {
				$url_args['slug'] .= '/' . $wc_qv[ $endpoint ];
			} else {
				$url_args['slug'] .= '/' . $wc_qv[ 'orders' ];
			}

			$url = cp_user_get_url( $url_args, $user );

			if ( ! empty( $args[ $endpoint ] ) && 'view-order' === $endpoint ) {
				if ( $has_pretty_link ) {
					$url = trailingslashit( $url ) . $wc_qv[ $endpoint ] . '/' . $args[ $endpoint ] ;
				} else {
					$url = add_query_arg( 'cpwoo_order', $args[ $endpoint ], $url );
				}
			}

			if ( ! empty( $args['orders'] ) && 'orders' === $endpoint ) {
				if ( $has_pretty_link ) {
					$url = trailingslashit( $url ) . $args['orders'];
				} else {
					$url = add_query_arg( 'paged', trim( str_replace( cp_get_paged_slug(), '', $args['orders'] ), '/' ), $url );
				}
			}
		} else {
			$url_args['slug'] .= '/' . cpwoo_get_user_dashboard_slug();
			$url = cp_user_get_url( $url_args, $user );
		}
	}

	/**
	 * Filter here to edit the redirect URL.
	 *
	 * @since 1.0.0
	 *
	 * @param string            An empty string if no matches were found.
	 *                           The ClusterPress profile url otherwise.
	 * @param array   $args     An array of arguments.
	 * @param string  $endpoint The requested woocommerce endpoint.
	 * @param WP_User $user     The User object to get the redirect url for.
	 */
	return apply_filters( 'cpwoo_redirect_my_account_url', $url, $args, $endpoint, $user );
}
